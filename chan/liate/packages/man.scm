(define-module (liate packages man)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages)
  #:use-module (gnu packages man)
  #:use-module ((nonguix licenses) #:prefix nl:))

(define-public posix-man-pages
  (package
    (inherit man-pages)
    (name "posix-man-pages")
    (version "2017")
    (source (origin
              (method url-fetch)
              (uri (string-append "mirror://kernel.org/linux/docs/man-pages/man-pages-posix/"
                                  "man-pages-posix-" version "-a.tar.xz"))
              (sha256 (base32 "1hwb65h9c7y8c69jiyhs0nphzisb7yl0br3jszd212q4nljvnryf"))))
    (build-system gnu-build-system)
    (arguments
     '(#:phases (modify-phases %standard-phases (delete 'configure))
       #:parallel-build? #f

       #:tests? #f
       #:make-flags (list (string-append "MANDIR="
                                         (assoc-ref %outputs "out")
                                         "/share/man"))))
    (synopsis "POSIX, as manpages, via the Linux project")
    (description
     "This package provides traditional Unix \"man pages\" documenting the POSIX
standard utilities and functions.")
    (license (nl:nonfree "file://POSIX-COPYRIGHT"))))
