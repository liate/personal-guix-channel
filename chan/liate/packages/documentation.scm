(define-module (liate packages documentation)
  #:use-module (guix packages)
  #:use-module ((guix licenses)
                #:prefix l:)
  #:use-module (guix build-system copy)
  #:use-module (guix download)
  #:use-module (gnu packages)
  #:use-module (gnu packages ocaml))

(define-public ocaml-info-manual
  (package
    (name "ocaml-info-manual")
    (version "5.0")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://ocaml.org/releases/"
                                  version
                                  "/ocaml-"
                                  version
                                  "-refman.info.tar.gz"))
              (sha256
               (base32
                "0hhpxl66slrcrgnjmr7bc853q7bdbyk4w3jz0slp10pbbz9p1inw"))))
    (build-system copy-build-system)
    (arguments
     (list #:install-plan ''(("./" "share/info/"))))
    (home-page (package-home-page ocaml))
    (synopsis "The OCaml Manual, as info files")
    (description "The OCaml manual, as info files")
    (license l:cc-by-sa4.0)))
