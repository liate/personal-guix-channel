(define-module (liate packages web)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system qt)
  #:use-module (guix build-system copy)
  #:use-module ((guix licenses) #:prefix l:)
  #:use-module (guix gexp)

  #:use-module (gnu packages qt)
  #:use-module (gnu packages dns)
  #:use-module (gnu packages base)
  #:use-module (gnu packages ccache)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages crypto)
  #:use-module (gnu packages ninja)
  #:use-module (gnu packages python)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages gcc)
  )

(define-public ladybird
  (let ((commit "107bfbe2830fe9ba18327ed36521d371ff0673db"))
    (package
      (name "ladybird")
      (version (git-version "0.0.0" "0" commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/SerenityOS/serenity/")
               (commit commit)))
         (sha256 (base32 "04mpy5gyh95ybpnfqi3sab3lmz8znxa8kswacgkha3bfy9yxpj32"))))
      (build-system qt-build-system)
      (arguments
       (list
        #:configure-flags
        #~(list "-DENABLE_NETWORK_DOWNLOADS=0"
                (string-append "-DTZDB_PATH=" #$tzdata "/share/zoneinfo/")
                (string-append "-DCLDR_PATH=" #$cldr-json "/share/cldr/")
                (string-append "-DCACERT_PATH=" #$curl-certs "/share/cacert/")
                "-DENABLE_PUBLIC_SUFFIX_DOWNLOAD=0")
        #:phases
        #~(modify-phases %standard-phases
            (add-after 'unpack 'chdir
              (lambda _
                (chdir "Ladybird/")
                #t)))))
      (native-inputs
       (list ccache
             cmake
             python
             gcc-12
             ninja
             pkg-config))
      (inputs
       (list cldr-json
             curl-certs
             ;; public-suffix-list
             libxcrypt
             qtbase
             qtmultimedia
             pulseaudio
             tzdata
             qtsvg
             qttools
             qtwayland))
      (synopsis "A new cross-platform browser, with an independent browser engine")
      (description
       "Ladybird is a web browser built on the LibWeb and LibJS engines from SerenityOS with a
cross-platform GUI in Qt.")
      (home-page "https://github.com/SerenityOS/serenity/tree/master/Ladybird")
      (license l:bsd-2))))

(define cldr-json
  (package
    (name "cldr-json")
    (version "44.1.0")
    (source
     (origin
      (method url-fetch)
      (uri
       (string-append
        "https://github.com/unicode-org/cldr-json/releases/download/" version
        "/cldr-" version "-json-modern.zip"))
      (sha256 (base32 "1vcrswgy6iq2ybqnmhr6f0in8z6sgbgm5nbk1hswjvrhy77ia072"))))
    (build-system copy-build-system)
    (native-inputs (list unzip))
    (arguments
     (list
      #:install-plan
      #~'(("." "share/cldr"))))
    (synopsis "JSON Data from the Unicode CLDR Project")
    (description
     "JSON distribution of CLDR locale data for internationalization")
    (home-page "https://github.com/unicode-org/cldr-json")
    (license l:unicode)))

;; TODO: make our own from standard nss-certs
(define curl-certs
  (package
    (name "curl-certs")
    (version "2023-12-12")
    (source
     (origin
       (method url-fetch)
       (uri
        (string-append "https://curl.se/ca/cacert-" version ".pem"))
       (sha256
        (base32 "0nmyzhrpfk2wx5gp5wf8wjpv26yg38ki0my1kjxvpmx0w4pzrgfc"))))
    (build-system copy-build-system)
    (arguments
     (list
      #:install-plan
      #~'(("." "share/cacert"))))
    (synopsis "CA certificates extracted from Mozilla")
    (description
     "The Mozilla CA certificate store in PEM format (around 200KB uncompressed)")
    (home-page "https://curl.se/docs/caextract.html")
    (license l:mpl2.0)))
