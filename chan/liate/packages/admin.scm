(define-module (liate packages admin)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages tls)
  #:use-module ((nonguix licenses) #:prefix nl:))

(define-public tarsnap
  (package
    (name "tarsnap")
    (version "1.0.40")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://www.tarsnap.com/download/tarsnap-autoconf-"
                                  version ".tgz"))
              (sha256 (base32 "1mbzq81l4my5wdhyxyma04sblr43m8p7ryycbpi6n78w1hwfbjmw"))))
    (build-system gnu-build-system)
    (inputs
     (list openssl
           zlib
           e2fsprogs))
    (arguments
     (list
      #:configure-flags
      #~(list (string-append "POSIX_SH=" #+bash "/bin/sh")
              (string-append "--with-zsh-completion-dir="
                             #$output "/share/zsh/site-functions/")
              (string-append "--with-bash-completion-dir="
                             #$output "/share/bash-completion/completions"))
      #:phases
      #~(begin
          (modify-phases %standard-phases
            (add-before 'configure 'fix-command-p
              (lambda _
                (substitute* (list "Makefile.in" "configure")
                  (("command -p ") ""))))))))
    (synopsis "Online backups for the truly paranoid")
    (description
     "Tarsnap is a secure, efficient online backup service:

@itemize @bullet
@item Encryption: your data can only be accessed with your personal keys.

  We can't access your data even if we wanted to!

@item Source code: the client code is available.

  You don't need to trust us; you can check the encryption yourself!

@item Deduplication: only the unique data between your current files and encrypted archives is uploaded.

  This reduces the bandwidth and storage required, saving you money!
@end itemize
Tarsnap runs on UNIX-like operating systems (BSD, Linux, macOS, Cygwin, etc).")
    (home-page "https://www.tarsnap.com/index.html")
    (license (nl:nonfree "https://raw.githubusercontent.com/Tarsnap/tarsnap/master/COPYING"))))
