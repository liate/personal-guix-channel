(define-module (liate packages bruijn)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system haskell)
  #:use-module ((guix licenses) #:prefix l:)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (gnu packages haskell-xyz)
  #:use-module (gnu packages haskell-check))

(define-public bruijn
  (let ((commit "a9960497f5e5a021066a8309fdfb6e53b852562f")
        (bare-version "0.1.0.0"))
    (package
      (name "bruijn")
      (version (git-version bare-version "1" commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/marvinborner/bruijn/")
               (commit commit)))
         (sha256
          (base32 "17q41mxahv39fq151xph243dfhab04bhj5rx2vg9yaj9whk7jyx7"))))
      (build-system haskell-build-system)
      (arguments
       (list
        #:phases
        #~(modify-phases %standard-phases
            (add-after 'install 'wrap-executable
              (lambda* (#:key inputs outputs #:allow-other-keys)
                (let ((out (assoc-ref outputs "out")))
                  (wrap-program (string-append out "/bin/bruijn")
                    `("bruijn_datadir" =
                      ;; This is wrong, but fixing it is a lot of effort
                      (,(string-append out
                                       "/share/x86_64-linux-ghc-9.2.5/bruijn-"
                                       #$bare-version
                                       "/"))))
                  #t))))))
      (inputs
       (list ghc-clock
             ghc-haskeline
             ghc-megaparsec
             ghc-random
             ghc-bitstring))
      (home-page "https://bruijn.marvinborner.de/")
      (synopsis
       "A purely functional programming language based on lambda calculus and De Bruijn indices written in Haskell")
      (description
       "Programming language based on pure bruijn-indexed lambda calculus and strong call-by-need reduction")
      (license l:expat))))

(define-public ghc-bitstring
  (package
    (name "ghc-bitstring")
    (version "0.0.0")
    (source
     (origin
       (method url-fetch)
       (uri (hackage-uri "bitstring" version))
       (sha256
        (base32 "1ix2x4v76wq5148k1aax69cf8sk14cd0z362dz1d2qmj9qxsnsw8"))))
    (build-system haskell-build-system)
    (properties '((upstream-name . "bitstring")))
    (inputs (list ghc-quickcheck))
    (home-page "http://code.haskell.org/~bkomuves/")
    (synopsis "Lazy bit strings")
    (description "Lazy bit strings, built on the top of bytestrings.")
    (license l:bsd-3)))
