(define-module (liate packages hare)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (gnu packages c)
  #:use-module (gnu packages man)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages cross-base)
  #:use-module (guix gexp)
  #:use-module ((guix licenses) #:prefix l:))

(define-public harec
  (let ((commit "b84c8ba483396a597fb57c5db9b0d764d2484632"))
    (package
     (name "harec")
     (version (git-version "0" "1" commit))
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://git.sr.ht/~sircmpwn/harec")
                    (commit commit)))
              (sha256 (base32 "0mqbhq5y0lw8f6q6fk7i0gz2dq98rfg4qnx4xv3hss7dlsb03rxn"))))
     (build-system gnu-build-system)
     (arguments
      (list
       #:phases
       #~(modify-phases %standard-phases
           (replace 'configure
             (lambda* (#:key inputs #:allow-other-keys)
               (invoke "env"
                       (string-append "CC=" #$(cc-for-target))
                       (string-append "LD=" #$(ld-for-target))
                       "./configure"
                       (string-append "--prefix=" #$output)))))))
     (inputs (list qbe))
     (native-inputs (list scdoc))
     (home-page "https://harelang.org/")
     (synopsis "A Hare compiler written in C11 for POSIX-compatible systems")
     (description
      "This is a Hare compiler written in C11 for POSIX-compatible systems.")
     (license l:gpl3))))

(define-public hare
  (let ((commit "e6a496014da5f4d5ad921983351b495b8bc43f92"))
    (package
      (name "hare")
      (version (git-version "0" "1" commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://git.sr.ht/~sircmpwn/hare")
                      (commit commit)))
                (sha256 (base32 "08q0jjg9gy83nw0iqyiz8jadgwn80dyn3jrm1zl3ikvwdfj1idhb"))))
      (build-system gnu-build-system)
      (arguments
       (list
        #:make-flags ; Replacement for `config.mk`.
        #~(let ((arch
                 (car (string-split (or #$(%current-target-system) #$(%current-system)) #\-))))
            (list
             (string-append "PREFIX = " #$output)
             "BINDIR = $(PREFIX)/bin"
             "MANDIR = $(PREFIX)/share/man"
             "SRCDIR = $(PREFIX)/share"

             "LOCALSRCDIR = $(SRCDIR)/hare"
             "HAREPATH = $(LOCALSRCDIR)/stdlib:$(LOCALSRCDIR)/third-party"
             
             "PLATFORM = linux"

             (string-append "ARCH = " arch)

             (string-append "HAREC = " #+(file-append harec "/bin/harec"))
             "HAREFLAGS = "
             (string-append "QBE =" #+(file-append qbe "/bin/qbe"))
             (string-append "AS = " #+(file-append gcc-toolchain "/bin/as"))
             (string-append "LD = " #+(ld-for-target))
             (string-append "AR = " #+(file-append gcc-toolchain "/bin/ar"))
             (string-append "SCDOC = " #+(file-append scdoc "/bin/scdoc"))

             "HARECACHE = .cache"
             "BINOUT = .bin"

             (string-append (string-upcase arch) "_AS = "
                            #$(file-append gcc-toolchain "/bin/as"))
             (string-append (string-upcase arch) "_AR = "
                            #$(file-append gcc-toolchain "/bin/ar"))
             (string-append (string-upcase arch) "_CC= "
                            #$(file-append gcc-toolchain "/bin/gcc"))
             (string-append (string-upcase arch) "_LD = "
                            #$(file-append gcc-toolchain "/bin/ld"))))
        #:phases
        #~(modify-phases %standard-phases
            (add-before 'configure 'no-config-mk
              (lambda _
                (substitute* "Makefile"
                  (("include config.mk") ""))))
            (delete 'configure)
            (add-after 'install 'make-wrapper
              (lambda* (#:key inputs outputs #:allow-other-keys)
                (let ((out (assoc-ref outputs "out")))
                  (wrap-program (string-append out "/bin/hare")
                    `("PATH" ":" prefix
                      ,(map (lambda (input)
                              (string-append (assoc-ref inputs input) "/bin"))
                            '("harec" "qbe"))))))))))
      (inputs (list harec qbe))
      (native-inputs (list harec scdoc))
      (native-search-paths
       (list
        (search-path-specification
         (variable "HAREPATH")
         (files (list "share/hare/stdlib" "share/hare/third-party")))))
      (home-page "https://harelang.org/")
      (synopsis "Hare is a systems programming language")
      (description
       "Hare is a systems programming language designed to be simple, stable, and robust. Hare
uses a static type system, manual memory management, and a minimal runtime. It is well-suited
to writing operating systems, system tools, compilers, networking software, and other
low-level, high performance tasks.")
      (license (list l:gpl3 l:mpl1.0)))))

(define-public powerctl
  (let ((commit "1c30fc759f5999718e810e69e7d05658030c1373"))
    (package
      (name "powerctl")
      (version (git-version "1" "1" commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://git.sr.ht/~sircmpwn/powerctl")
                      (commit commit)))
                (sha256 (base32 "09l8s2qxcp2mhbsjzw3skk2ng7cpkl88by2lwqbirpf1bxrgg0g1"))))
      (build-system gnu-build-system)
      (arguments
       (list
        #:tests? #f
        #:make-flags
        #~(list
           (string-append "PREFIX = " #$output))
        #:phases
        #~(modify-phases %standard-phases
            (add-before 'configure 'set-harecache
              (lambda _
                (setenv "HARECACHE" "./hare-cache")))
            (delete 'configure))))
      (inputs (list hare))
      (native-inputs (list scdoc hare))
      (home-page "https://git.sr.ht/~sircmpwn/powerctl")
      (synopsis "A simple command line utility to control power states on Linux, i.e. to suspend or hibernate the system")
      (description "A simple command line utility to control power states on Linux, i.e. to
suspend or hibernate the system")
      (license l:gpl3))))
