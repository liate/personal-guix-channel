(define-module (liate packages wayland)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix build-system meson)
  #:use-module ((guix licenses) #:prefix l:)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages pkg-config))

(define-public wljoywake
  (package
    (name "wljoywake")
    (version "v0.3")
    (source
     (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/nowrep/wljoywake")
         (commit version)))
       (sha256
        (base32
         "0wa7qdh0k5hp3hmx0al0zqmwvkhb0crj1rckbn90dcv3q5z0s9nd"))))
    (build-system meson-build-system)
    (inputs
     (list wayland wayland-protocols eudev))
    (native-inputs
     (list pkg-config))
    (home-page "https://github.com/nowrep/wljoywake")
    (synopsis "Wayland idle inhibit on joystick input")
    (description
     "Wayland tool for idle inhibit when using joysticks. Requires idle_inhibit_unstable_v1 protocol.

Usage: wljoywake [options]

Options:
  -t TIMEOUT_SECONDS    Set idle wakeup timeout
  -h --help             Show this help message
  -v --version          Show version

")
    (license l:gpl2)))
