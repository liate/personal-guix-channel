(define-module (liate packages pony)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  #:use-module (guix search-paths)
  #:use-module ((guix licenses) #:prefix l:)

  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages check)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages crypto)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages python)

  #:use-module (srfi srfi-26)
  #:use-module (ice-9 match))

(define googletest-1.12
  (package
    (inherit googletest)
    (version "1.12.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/google/googletest")
                    (commit (string-append "release-" version))))
              (file-name (git-file-name "googletest" version))
              (sha256 (base32 "1cv55x3amwrvfan9pr8dfnicwr8r6ar3yf6cg9v6nykd6m2v3qsv"))))))

(define googlebenchmark-1.7
  (package
    (inherit googlebenchmark)
    (version "1.7.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference (url "https://github.com/google/benchmark")
                                  (commit (string-append "v" version))))
              (file-name (git-file-name "google-benchmark" version))
              (sha256 (base32 "0fpvjka5dp8lza87mny7v3s016mksd8gdbx239kx3nx28bzy03c2"))))))

(define llvm-for-ponyc
  (package
    (inherit llvm-14)
    (version "14.0.3")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/llvm/llvm-project")
                    (commit (string-append "llvmorg-" version))))
              (sha256 (base32 "0makhpbrg46m2gi8wyp5h21ln4mgilahh3clk4d1b2ln2ck3v7m8"))))
    (arguments ;; Mostly copied from llvm-14
     (list
      #:configure-flags
      #~(list
         ;; These options are required for cross-compiling LLVM according
         ;; to <https://llvm.org/docs/HowToCrossCompileLLVM.html>.
         #$@(if (%current-target-system)
                #~((string-append "-DLLVM_TABLEGEN="
                                  #+(file-append this-package
                                                 "/bin/llvm-tblgen"))
                   #$(string-append "-DLLVM_DEFAULT_TARGET_TRIPLE="
                                    (%current-target-system))
                   #$(string-append "-DLLVM_TARGET_ARCH="
                                    (system->llvm-target))
                   #$(string-append "-DLLVM_TARGETS_TO_BUILD="
                                    (system->llvm-target)))
                '())
         "-DCMAKE_SKIP_BUILD_RPATH=FALSE"
         "-DCMAKE_BUILD_WITH_INSTALL_RPATH=FALSE"
         "-DBUILD_SHARED_LIBS:BOOL=TRUE"
         "-DLLVM_ENABLE_FFI:BOOL=TRUE"
         "-DLLVM_REQUIRES_RTTI=1"       ;for some third-party utilities
         "-DLLVM_INSTALL_UTILS=ON"
         ;; These are the targets ponyc needs
         "-DLLVM_TARGETS_TO_BUILD=X86;ARM;AArch64;WebAssembly;RISCV")
      #:build-type "Release"
      #:phases
      #~(modify-phases %standard-phases
          (add-after 'unpack 'change-directory
            (lambda _
              (chdir "llvm")))
          (add-after 'install 'install-opt-viewer
            (lambda* (#:key outputs #:allow-other-keys)
              (let* ((out (assoc-ref outputs "out"))
                     (opt-viewer-out (assoc-ref outputs "opt-viewer"))
                     (opt-viewer-share-dir (string-append opt-viewer-out "/share"))
                     (opt-viewer-dir (string-append opt-viewer-share-dir "/opt-viewer")))
                (mkdir-p opt-viewer-share-dir)
                (rename-file (string-append out "/share/opt-viewer")
                             opt-viewer-dir)))))))))

(define-public ponyc
  (package
    (name "ponyc")
    (version "0.53.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/ponylang/ponyc/")
                    (commit version)))
              (sha256 (base32 "0spkha7s5za9vwl6x1cxws8fm9k331hry9yi3mi7xxcpfbk8qfmd"))
              (patches (list (local-file "./patches/ponyc-no-vendoring.patch")
                             (local-file "./patches/ponyc-disable-sandboxed-tests.patch")
                             (local-file "./patches/ponyc-disable-too-slow-test.patch")))))
    (build-system gnu-build-system)
    (arguments
     (list
      #:test-target "test"
      #:parallel-tests? #f
      #:strip-binaries? #f
      #:configure-flags
      #~(list
         "lto=yes"
         "pic_flag=-fPIC"
         (string-append "CMAKE_FLAGS="
                        "-DCMAKE_CXX_FLAGS=-pthread"))
      #:phases
      #~(modify-phases %standard-phases
          (add-before 'configure 'set-env
            (lambda* (#:key inputs #:allow-other-keys)
              (setenv "CC" #$(cc-for-target))
              (setenv "CXX" #$(cxx-for-target))))
          (add-before 'configure 'libs
            (lambda* (#:key inputs #:allow-other-keys)
              (invoke "make" "libs")))
          (replace 'configure
            (lambda* (#:key configure-flags #:allow-other-keys)
              (apply invoke "make" "configure"
                     configure-flags)))
          (add-before 'check 'patch-process-tests
            (lambda* (#:key inputs #:allow-other-keys)
              (substitute* "packages/process/_test.pony"
                (("/bin/pwd")
                 #+(file-append coreutils "/bin/pwd")))))
          (replace 'install
            (lambda* (#:key outputs #:allow-other-keys)
              (invoke "make" "install" (string-append "prefix=" #$output))))
          (add-after 'install 'wrap-ponyc
            (lambda* (#:key outputs #:allow-other-keys)
              (wrap-program (string-append (assoc-ref outputs "out") "/bin/ponyc")
                '("PATH" ":" prefix
                  (#$(file-append ld-gold-wrapper "/bin")
                   #$(file-append gcc-toolchain "/bin")))
                '("LIBRARY_PATH" ":" suffix
                  (#$(file-append gcc-toolchain "/lib")))))))))
    (inputs (list llvm-for-ponyc
                  `(,gcc "lib") libffi
                  googletest-1.12
                  googlebenchmark-1.7
                  ;; For the wrapper
                  gcc-toolchain ld-gold-wrapper))
    (native-inputs (list cmake python ld-gold-wrapper))
    (native-search-paths
     (cons* (search-path-specification
             (variable "PONYPATH")
             ;; Pony doesn't seem to have an established convention for where installed
             ;; libraries go.
             ;; Corral, pony's package manager, just downloads the sources to
             ;; ~/.corral/repo-cache and $PROJECT_ROOT/_corral, so is no help.
             ;; The standard library is automatically included, but is at
             ;; $PREFIX/lib/pony/$VERSION/packages/.  The 'packages' seem unnecessary,
             ;; but means that ponyc starts with a valid PONYPATH.  Having the stdlib
             ;; in PONYPATH seems not to break anything.
             (files (list (string-append "lib/pony/" version "/packages/"))))
            ;; Because it invokes gcc
            (package-native-search-paths gcc-toolchain)))
    (home-page "https://www.ponylang.io/")
    (synopsis
     "An open-source, actor-model, capabilities-secure, high performance programming language")
    (description
     "Pony is an open-source, object-oriented, actor-model, capabilities-secure,
high-performance programming language.

What makes Pony different?
Pony is type, memory, and exception safe;
data-race and deadlock free; and
compiles to native code with built-in C compatibility.")
    (license l:bsd-2)))
