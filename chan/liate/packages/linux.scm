(define-module (liate packages linux)
  #:use-module (guix packages)
  #:use-module (nongnu packages linux)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages))

#;(define-public linux-arm64-generic-5.19.8
  (corrupt-linux (package
                   (inherit
                    (customize-linux #:linux linux-libre-arm64-generic))
                   (version "5.19.8"))
	             "linux-arm64-generic"))
