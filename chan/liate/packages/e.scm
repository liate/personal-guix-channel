(define-module (liate packages e)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  #:use-module (guix build-system gnu)
  #:use-module (guix modules)
  #:use-module (gnu packages)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages java)
  #:use-module ((guix licenses) #:prefix l:)
  #:use-module (srfi srfi-26))

(define-public elang
  (package
    (name "elang")
    (version "0.9.3d")
    (source (origin
              (method url-fetch)
              (uri (string-append "http://www.erights.org/download/0-9-3/"
                                  "E-src-" version ".tar.gz"))
              (sha256 (base32 "1jqzp23ilp07h8qfjvb6wx7ba8gf8i4256ssjcwyh1gxh1i146jb"))))
    (build-system gnu-build-system)
    (inputs
     (list `(,icedtea-8 "jdk")
           java-swt))
    (native-inputs
     (list findutils which))
    (propagated-inputs
     (list java-swt))
    (arguments
     (list
      #:make-flags
      #~(list (string-append "SHELL=" #$bash "/bin/bash")
              "OSDIR=linux-gtk")
      #:parallel-build? #f
      #:tests? #f
      #:phases
      (with-imported-modules (source-module-closure
                              '((ice-9 regex)
                                (srfi srfi-26)))
          #~(modify-phases %standard-phases
              (replace 'configure
                (lambda* _
                  (chdir "src")))
              (replace 'install
                (lambda* (#:key inputs outputs #:allow-other-keys)
                  (use-modules (ice-9 regex) (srfi srfi-26))
                  (let* ((out (assoc-ref outputs "out"))
                         (e-home (string-append out "/share/e/"))
                         (build "../export/dist/"))
                    (substitute* (map (λ (<>)
                                        (string-append build <>))
                                      '("eprops-template.txt" "rune-template.txt"
                                        "devrune-template.txt"))
                      (((regexp-quote "${{e.home}}"))
                       e-home)
                      (((regexp-quote "${{e.javacmd}}"))
                       #$(file-append icedtea-8 "/bin/java"))
                      (((regexp-quote "${{e.launch.dir}}"))
                       "")
                      (((regexp-quote "${{TraceLog_dir}}"))
                       "/tmp/etrace")
                      (((regexp-quote "${{e.put.bash.pathlist}}"))
                       (string-append out "/bin/"))
                      (((regexp-quote "${{e.put.shortcut.pathlist}}"))
                       ""))
                    (rename-file (string-append build "eprops-template.txt")
                                 (string-append build "eprops.txt"))
                    (rename-file (string-append build "rune-template.txt")
                                 (string-append build "rune"))
                    (install-file (string-append build "rune")
                                  (string-append out "/bin/"))
                    (rename-file (string-append build "devrune-template.txt")
                                 (string-append build "devrune"))
                    (mkdir-p e-home)
                    (copy-recursively "../export/dist" e-home))))))))
    (synopsis
     "An object-capability programming language")
    (description
     "E is an object-capability programming language and platform for writing distributed,
secure, and robust software

Currently does not support graphics, for unknown reasons")
    (home-page "http://www.erights.org/")
    (license (list l:mpl1.1
                   l:bsd-3
                   l:bsd-4
                   l:asl1.1
                   l:w3c))))
