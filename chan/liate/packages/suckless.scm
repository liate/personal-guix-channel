(define-module (liate packages suckless)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages)
  #:use-module (gnu packages image)
  #:use-module (guix utils)
  #:use-module ((guix licenses) #:prefix l:))

(define-public farbfeld
  (package
    (name "farbfeld")
    (version "4")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://dl.suckless.org/farbfeld/farbfeld-"
                                  version ".tar.gz"))
              (sha256 (base32 "0ap7rcngffhdd57jw9j22arzkbrhwh0zpxhwbdfwl8fixlhmkpy7"))))
    (build-system gnu-build-system)
    (inputs
     (list libpng libjpeg-turbo))
    (arguments
     `(#:tests? #f
       #:make-flags (list (string-append "CC=" ,(cc-for-target))
                          (string-append "PREFIX=" %output))
       #:phases
       (modify-phases %standard-phases
         (delete 'configure))))
    (synopsis "A lossless image format which is easy to parse, pipe and compress")
    (description
     "Farbfeld is a lossless image-format designed to be parsed and piped
easily. It is probably the simplest image-format you can find (see FORMAT).  It
does not have integrated compression, but allows compression algorithms to work
with it easily by adding little entropy to the image data itself.  This beats PNG
in many cases.  Given the free choice of compression algorithms, it is trivial
to switch to better and faster ones as they show up in the future.")
    (license l:isc)
    (home-page "http://tools.suckless.org/farbfeld/")))
