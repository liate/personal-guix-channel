(define-module (liate packages firmware)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system copy)
  #:use-module ((guix licenses) #:prefix l:)
  #:use-module (gnu packages)
  #:use-module (nonguix licenses))

(define-public brcm-supplemental-firmware
  (package
    (name "brcm-supplemental-firmware")
    (version "1.3")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/bsdkurt/brcm-supplemental")
                    (commit (string-append "brcm-supplemental-" version))))
              (sha256
               (base32 "09m5whfg4a9rrk7la8jai2bylgwcb2g04530xkjz9x31ghn9v2p7"))))
    (build-system copy-build-system)
    (arguments
     `(#:install-plan
       '(("." "lib/firmware/brcm/" #:include-regexp (".*bin" ".*txt"))
         ("." ,(string-append "share/doc/" name "-" version "/")
          #:include ("LICENSE")))))
    (home-page "https://github.com/bsdkurt/brcm-supplemental/")
    (synopsis "Extra firware for Broadcom brcm devices")
    (description "Board specific firmware & NVRAM for Broadcom brcm devices.
File names have been renamed to be board specific which aligns with how OpenBSD's
bwfm driver loads them.")
    (license
     (nonfree "file://LICENSE"))))

(define-public pinebook-pro-wifi-firmware
  (let ((commit "6790a16667e9602300a850e532de4e9b6a00eada"))
    (package
      (name "pinebook-pro-wifi-firmware")
      (version (git-version "1" "0" commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://gitlab.com/kalilinux/build-scripts/kali-arm")
                      (commit commit)))
                (sha256
                 (base32 "0azkqrg276777vjnbqhh67vy9ka3zl544j3b5gb1h6pqmpdcmg70"))))
      (build-system copy-build-system)
      (arguments
       `(#:install-plan
         '(("bsp/firmware/pbp" "lib/firmware/brcm"))))
      (home-page "https://gitlab.com/kalilinux/build-scripts/kali-arm/-/issues/286#note_1069959116")
      (synopsis "Kali's firware for the Pinebook Pro")
      (description "Kali's firware for the Pinebook Pro")
      (license
       (nonfree "https://github.com/bsdkurt/brcm-supplemental/blob/brcm-supplemental-1.3/LICENSE")))))
