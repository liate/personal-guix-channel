(define-module (liate packages emacs)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system emacs)
  #:use-module (guix build-system gnu)
  #:use-module (guix build utils)
  #:use-module (guix utils)
  #:use-module ((guix licenses) #:prefix l:)
  #:use-module (gnu packages)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages scheme)
  #:use-module (gnu packages web)
  #:use-module (gnu packages webkit))

(define-public emacs-eshell-fringe-status
  (package
    (name "emacs-eshell-fringe-status")
    (version "20170117.2316")
    (source
     (origin
       (method url-fetch)
       (uri
        (string-append
         "https://melpa.org/packages/eshell-fringe-status-"
         version
         ".el"))
       (sha256
        (base32
         "0wjf2h7llm9rilx3bqgm2640770gv0jxrwipd4kydb7ng2dnjmwd"))))
    (build-system emacs-build-system)
    (home-page
     "http://projects.ryuslash.org/eshell-fringe-status/")
    (synopsis "Show last status in fringe")
    (description
     "Show an indicator of the status of the last command run in eshell.
To use, enable `eshell-fringe-status-mode' in `eshell-mode'.  The
easiest way to do this is by adding a hook:

: (add-hook 'eshell-mode-hook 'eshell-fringe-status-mode)

This mode uses a rather hackish way to try and keep everything
working in regard to `eshell-prompt-regexp', so if anything breaks
please let me know.

Some extra fringe bitmaps are provided.  In case you prefer either
or both of them over the default arrow bitmap.  These are
`efs-plus-bitmap' and `efs-minus-bitmap'.  These show a `+' and `-'
in the fringe respectively, instead of an arrow.  These can be used
by setting the `eshell-fringe-status-success-bitmap' and the
`eshell-fringe-status-failure-bitmap' options.
")
    (license #f)))

(define-public emacs-xah-css-mode
  (package
    (name "emacs-xah-css-mode")
    (version "20210627.505")
    (source
     (origin
       (method url-fetch)
       (uri
        (string-append
         "https://melpa.org/packages/xah-css-mode-"
         version
         ".el"))
       (sha256
        (base32
         "0zsqnvvzv0pd50r973scsj202yn9hdffqwbwqn2h0rscibrbs6y2"))))
    (build-system emacs-build-system)
    (home-page
     "http://ergoemacs.org/emacs/xah-css-mode.html")
    (synopsis "Major mode for editing CSS code.")
    (description
     "Major mode for editing CSS code. Alternative to GNU emacs's builtin `css-mode'.

Features:

• Correct Syntax coloring ALL CSS words. Does not color typos.

• Color coded words by semantics. Each type of CSS words are colored distinctly. e.g. HTML tag names, CSS attribute names, predefined CSS value names, CSS unit names, pseudo selector names, media keywords, etc.

• ID selector string and class name in bold for easy identification.

• Keyword completion with `ido-mode' interface. Press Tab ↹ after a word to complete. All CSS words are supported: {html5 tags, property names, property value keywords, units, colors, pseudo selectors, “at keywords”, …}.

• Single Key Prettify Code Format. Press Tab ↹ before word to reformat current block of code. That is, all lines enclosed by curly brackets {}.

• Syntax coloring of hexadecimal color format #rrggbb , #rgb , and HSL Color format hsl(0,68%,42%).

• Call `xah-css-hex-to-hsl-color' to convert #rrggbb color format under cursor to HSL Color format.

• Call `xah-css-compact-css-region' to minify region.

• Call `xah-css-expand-to-multi-lines' to expand minified CSS code to multi-lines format.

• Call `describe-function' on `xah-css-mode' for detail.
")
    (license #f)))

(define-public emacs-j-mode
  (package
    (name "emacs-j-mode")
    (version "20171224.1856")
    (source
     (origin
       (method url-fetch)
       (uri
        (string-append
         "https://melpa.org/packages/j-mode-"
         version
         ".tar"))
       (sha256
        (base32
         "0cl6r0xihgmy0mjx7ja5ahfjdh65vns8i1jh583v3l7zv3iiri52"))))
    (build-system emacs-build-system)
    (home-page "http://github.com/zellio/j-mode")
    (synopsis "Major mode for editing J programs")
    (description
     "Provides font-lock and basic REPL integration for the
[J programming language](http://www.jsoftware.com)

; Installation

The only method of installation is to check out the project, add it to the
load path, and load normally. This may change one day.

Put this in your emacs config
  (add-to-list 'load-path \"/path/to/j-mode/\")
  (load \"j-mode\")

Add for detection of j source files if the auto-load fails
  (add-to-list 'auto-mode-alist '(\"\\\\.ij[rstp]$\" . j-mode)))
")
    (license #f)))

(define-public emacs-message-view-patch
  (package
    (name "emacs-message-view-patch")
    (version "20210904.2227")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://melpa.org/packages/message-view-patch-"
             version
             ".el"))
       (sha256
        (base32
         "1205qxvbfipdy8f2lscg1f1qk59j444gmj3642f4acnwd65nsnr8"))))
    (build-system emacs-build-system)
    (propagated-inputs
     `(("emacs-magit" ,emacs-magit)))
    (home-page
     "https://github.com/seanfarley/message-view-patch")
    (synopsis "Colorize patch-like emails in mu4e")
    (description
     "This is adapted from Frank Terbeck's gnus-article-treat-patch.el but has
been adapted to work with mu4e.
")
    (license #f)))

(define-public emacs-elfeed-web
  (package
   (name "emacs-elfeed-web")
   (version "20210226.258")
   (source
    (origin
     (method url-fetch)
     (uri (string-append
           "https://melpa.org/packages/elfeed-web-"
           version
           ".tar"))
     (sha256
      (base32 "1j9381f6jg3lbxjyj1b5pysblypypzgb49fb0x5hgxd09c9rj0w1"))))
   (build-system emacs-build-system)
   (arguments
    `(#:phases
      (modify-phases %standard-phases
        (add-after 'install 'install-static-files
          (lambda* (#:key outputs #:allow-other-keys)
            (let* ((out (assoc-ref outputs "out"))
                   (target (string-append out "/share/emacs/site-lisp/elfeed-web-"
                                          ,version
                                          "/")))
              (for-each
               (lambda (filename)
                 (install-file filename target))
               '("elfeed.css" "elfeed.js" "index.html"))))))))
   (propagated-inputs
    `(("emacs-simple-httpd" ,emacs-simple-httpd)
      ("emacs-elfeed" ,emacs-elfeed)))
   (home-page "https://github.com/skeeto/elfeed")
   (synopsis "web interface to Elfeed")
   (description
    "This is a very early work in progress. The long-term goal is to
provide a web interface view of the database with optional remote
tag updating. An AngularJS client accesses the database over a few
RESTful endpoints with JSON for serialization.

The IDs provided by RSS and Atom are completely arbitrary. To avoid
ugly encoding issues they're normalized into short, unique,
alphanumeric codes called webids. Both feeds and entries fall into
the same webid namespace so they share a single endpoint.

Endpoints:

/elfeed/<path>
    Serves the static HTML, JS, and CSS content.

/elfeed/content/<ref-id>
    Serves content from the content database (`elfeed-deref').

/elfeed/things/<webid>
    Serve up an elfeed-feed or elfeed-entry in JSON format.

/elfeed/search
    Accepts a q parameter which is an filter string to be parsed
    and handled by `elfeed-search-parse-filter'.

/elfeed/tags
    Accepts a PUT request to modify the tags of zero or more
    entries based on a JSON entry passed as the content.

/elfeed/update
    Accepts a time parameter. If time < `elfeed-db-last-update',
    respond with time. Otherwise don't respond until database
    updates (long poll).
")
   (license #f)))

(define-public emacs-zig-mode
  (package
   (name "emacs-zig-mode")
   (version "20210831.719")
   (source
    (origin
     (method url-fetch)
     (uri (string-append
           "https://melpa.org/packages/zig-mode-"
           version
           ".el"))
     (sha256
      (base32 "057cmbzqsih86rfb80pvp37l233rp0irjysyrd009yqbj414rrdh"))))
   (build-system emacs-build-system)
   (home-page "https://github.com/zig-lang/zig-mode")
   (synopsis "A major mode for the Zig programming language")
   (description
    "A major mode for the Zig programming languages.

See documentation on https://github.com/zig-lang/zig-mode
")
   (license #f)))

(define emacs-wisi
  (package
    (name "emacs-wisi")
    (version "4.2.1")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://elpa.gnu.org/packages/wisi-" version
                                  ".tar"))
              (sha256
               (base32
                "1ndvcr0gfv3xrzvzawh3miz3ppi1mkdzkp54l6hxxh174r88pmx4"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-seq))
    (home-page "https://stephe-leake.org/ada/wisitoken.html")
    (synopsis
     "Utilities for implementing an indentation/navigation engine using a generalized LR parser")
    (description
     "Emacs wisi package 4.2.0 The wisi package provides utilities for using
generalized error-correcting LR parsers (in external processes) to do
indentation, fontification, and navigation; and integration with Emacs
package.el.  See ada-mode for an example of its use.  It also provides
wisitoken-parse_table-mode, for navigating the diagnostic parse tables output by
wisitoken-bnf-generate.  The generated code is in Ada; it can be built via Alire
(https://alire.ada.dev/).  Normally this is done by a package that uses wisi,
such as ada-mode.")
    (license l:gpl3+)))

(define emacs-uniquify-files
  (package
    (name "emacs-uniquify-files")
    (version "1.0.4")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "https://elpa.gnu.org/packages/uniquify-files-" version
                    ".tar"))
              (sha256
               (base32
                "0ry52l9p2sz8nsfh15ffa25s46vqhna466ahmjmnmlihgjhdm85q"))))
    (build-system emacs-build-system)
    (home-page "https://elpa.gnu.org/packages/uniquify-files.html")
    (synopsis "Completion style for files, minimizing directories")
    (description "No description available.")
    (license l:gpl3+)))

(define emacs-gnat-compiler
  (package
    (name "emacs-gnat-compiler")
    (version "1.0.1")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://elpa.gnu.org/packages/gnat-compiler-"
                                  version ".tar"))
              (sha256
               (base32
                "0x4f6ilmfkzg3dgslbkkzaaxqbyvryb09v0j82a3q1sdf938zsql"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-wisi))
    (home-page "https://elpa.gnu.org/packages/gnat-compiler.html")
    (synopsis "Support for running GNAT tools")
    (description "No description available.")
    (license l:gpl3+)))

(define-public emacs-ada-mode
  (package
    (name "emacs-ada-mode")
    (version "8.0.5")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://elpa.gnu.org/packages/ada-mode-"
                                  version ".tar"))
              (sha256
               (base32
                "00baypl9bv2z42d6z2k531ai25yw2aj1dcv4pi1jhcp19c9kjg4l"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-uniquify-files emacs-wisi emacs-gnat-compiler emacs-eglot))
    (home-page "http://www.nongnu.org/ada-mode/")
    (synopsis "major-mode for editing Ada sources")
    (description
     "Emacs Ada mode version 8.0.1 Ada mode provides auto-casing, fontification,
navigation, and indentation for Ada source code files.  Cross-reference via
Emacs xref can use an xref backend provided by the gpr-query package, or a
language server via the eglot package; they must be installed separately.  Ada
mode uses whichever of these is found on PATH, defaulting to gpr-query.  Ada
mode uses a parser to provide fontification, single-file navigation, and
indentation.  Ada mode allows using eglot as the backend for these, but the
current version of AdaCore ada_language_server only supports single and
multi-file navigation.  The wisi parser backend supports all Ada mode functions,
is implemented in Ada, is fast enough even for very large files (via partial or
incremental parsing), and recovers from almost all syntax errors.  The wisi
parser is provided as Ada source code that must be compiled and installed,
either directly or via Alire (https://alire.ada.dev/): cd
~/.emacs.d/elpa/ada-mode-i.j.k ./build.sh ./install.sh install.sh can take an
option \"--prefix=<dir>\" to set the installation directory.  Both shell scripts
use Alire if the alr executable is found in PATH. Ada mode will be automatically
loaded when you open a file with a matching extension (default *.ads, *.adb).
Ada mode uses project files to define large (multi-directory) projects, and to
define casing exceptions.  See ada-mode.info for help on using and customizing
Ada mode.")
    (license l:gpl3+)))

(define-public emacs-yafolding
  (package
    (name "emacs-yafolding")
    (version "20200119.1353")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/emacsorphanage/yafolding.git")
                    (commit "4c1888ae45f9241516519ae0ae3a899f2efa05ba")))
              (sha256
               (base32
                "1bb763lx5cs5z06irjllip8z9c61brjsamfcjajibi24wcajkprx"))))
    (build-system emacs-build-system)
    (home-page "unspecified")
    (synopsis "Folding code blocks based on indentation")
    (description "Folding code blocks based on indentation.")
    (license #f)))

(define-public emacs-company-ctags
  (package
    (name "emacs-company-ctags")
    (version "20211211.338")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/redguardtoo/company-ctags.git")
                    (commit "313508ba5d4f1e4b5d5d554faaa74076201c3248")))
              (sha256
               (base32
                "0hf7lq6rcs6xhmgpc8bwk115rykyfvakcjqpanlsh5m3fdswjq03"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-company))
    (home-page "https://github.com/redguardtoo/company-ctags")
    (synopsis "Fastest company-mode completion backend for ctags")
    (description
     "This library completes code using tags file created by Ctags.  It uses a much
faster algorithm optimized for ctags.  It takes only 9 seconds to load 300M tags
file which is created by scanning the Linux Kernel code v5.3.1.  After initial
loading, this library will respond immediately when new tags file is created.
Usage: Step 0, Make sure `company-mode is already set up See
http://company-mode.github.io/ for details.  Step 1, insert below code into your
configuration, (with-eval-after-load company (company-ctags-auto-setup)) Step 2,
Use Ctags to create tags file and enjoy.  Tips: - Turn on
`company-ctags-support-etags to support tags file created by etags.  But it will
increase initial loading time. - Set `company-ctags-extra-tags-files to load
extra tags files, (setq company-ctags-extra-tags-files (\"$HOME/TAGS\"
\"/usr/include/TAGS\")) - Set `company-ctags-fuzzy-match-p to fuzzy match the
candidates.  The input could match any part of the candidate instead of the
beginning of the candidate. - Set `company-ctags-ignore-case to ignore case when
fetching candidates - Use rusty-tags to generate tags file for Rust programming
language.  Add below code into ~/.emacs, (setq company-ctags-tags-file-name
\"rusty-tags.emacs\") - Make sure CLI program diff is executable on Windows.  It's
optional but highly recommended.  It can speed up tags file updating.  This
package uses diff through variable `diff-command'. - `company-ctags-debug-info
for debugging.")
    (license #f)))

(define-public emacs-ponylang-mode
  (package
    (name "emacs-ponylang-mode")
    (version "20211015.331")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/ponylang/ponylang-mode.git")
                    (commit "0c0c707b440dfb2e628ecf4ff2c78e31518caa5b")))
              (sha256
               (base32
                "1rjc3j02y82cdzyp85gli5wyq5dhicay8k6wcxkyzb5lhnwpvpk3"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-dash
                             emacs-hydra
                             emacs-hl-todo
                             emacs-yafolding
                             emacs-yasnippet
                             emacs-company-ctags
                             emacs-rainbow-delimiters
                             emacs-fill-column-indicator))
    (home-page "https://github.com/ponylang/ponylang-mode")
    (synopsis "A major mode for the Pony programming language")
    (description
     "Description: This is a major mode for the Pony programming language For more
details, see the project page at https://github.com/ponylang/ponylang-mode
Installation: The simple way is to use package.el: M-x package-install
ponylang-mode Or, copy ponylang-mode.el to some location in your Emacs load
path.  Then add \"(require ponylang-mode)\" to your Emacs initialization (.emacs,
init.el, or something).  Example config: (require ponylang-mode)")
    (license #f)))

(define-public emacs-flycheck-pony
  (package
    (name "emacs-flycheck-pony")
    (version "20210118.1327")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/ponylang/flycheck-pony.git")
                    (commit "50c802b9c0a1f1e583061f8c5334847463e4adb2")))
              (sha256
               (base32
                "04hk2njj4bd6q9zmb377dk00mlvn6kx5jrz3gbd21fllwh4p6bd2"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-flycheck))
    (home-page "https://github.com/seantallen/flycheck-pony")
    (synopsis "Pony support in Flycheck")
    (description
     "Pony syntax checking support for Flycheck.  Runs \"ponyc -rfinal\" in the current
working directory.  You may need to customize the location of your Pony compiler
if Emacs isn't seeing it on your PATH: (setq flycheck-pony-executable
\"/usr/local/bin/ponyc\")")
    (license #f)))

(define-public emacs-sway-msg
  (let ((commit "5b4b0d1bbd62020677503e524116dd6bb5c6180b"))
    (package
      (name "emacs-sway-msg")
      (version (git-version "0.0" "1" commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://gitlab.com/liate/emacs-sway-msg")
                      (commit commit)))
                (sha256
                 (base32 "1nmncpi121a00kpriczry8011dp14cqfb8f2n6ax7faqixl2dsw1"))))
      (build-system copy-build-system)
      (arguments
       (list
        #:install-plan ''(("." "share/emacs/site-lisp/" #:include-regexp (".*\\.el"))
                          ("emacs-sway-msg-proxy" "bin/"))))
      (propagated-inputs (list jq))
      (synopsis "Some scripts for combining emacs and sway")
      (description
       "Scripts for controlling emacs with sway keybinds.  Currently requires swaymsg and
emacsclient in your PATH.")
      (home-page "https://gitlab.com/liate/emacs-sway-msg")
      (license l:cc-by4.0))))

(define-public emacs-e-mode
  (package
    (name "emacs-e-mode")
    (version "0.9.3d")
    (source (origin
              (method url-fetch)
              (uri (string-append "http://www.erights.org/download/0-9-3/"
                                  "E-src-" version ".tar.gz"))
              (sha256 (base32 "1jqzp23ilp07h8qfjvb6wx7ba8gf8i4256ssjcwyh1gxh1i146jb"))))
    (build-system copy-build-system)
    (arguments
     (list
      #:install-plan
      #~'(("src/elisp/" "share/emacs/site-lisp" #:include-regexp (".*\\.el")))))
    (synopsis "An emacs mode for highlighting (and executing) E code")
    (description
     "Emacs code for editing and eval'ing E code")
    (home-page "http://www.erights.org/")
    (license l:public-domain)))

(define-public emacs-xwwp
  (package
    (name "emacs-xwwp")
    (version "20200917.643")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/canatella/xwwp.git")
                    (commit "f67e070a6e1b233e60274deb717274b000923231")))
              (sha256 (base32
                       "1ikhgi3gc86w7y3cjmw875c8ccsmj22yn1zm3abprdzbjqlyzhhg"))))
    (build-system emacs-build-system)
    (arguments '(#:include '("^xwwp.el$" "^xwwp-follow-link.el$"
                             "^xwwp-follow-link-ido.el$")
                 #:exclude '()))
    (home-page "https://github.com/canatella/xwwp")
    (synopsis "Enhance xwidget webkit browser")
    (description
     "This package provides the common functionnality for other xwidget webkit plus
packages.  It provides the customize group and a framework to inject css and
javascript functions into an `xwidget-webkit session.")
    (license l:gpl3)))

(define-public emacs-ocaml-eglot
  (package
    (name "emacs-ocaml-eglot")
    (version "1.0.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://github.com/tarides/ocaml-eglot/archive/refs/tags/" version ".tar.gz"))
       (sha256
        (base32 "1256igax7985ih5qw1bp3z11glmv1b5lslr4pfi488jwj1j4880d"))))
    (build-system emacs-build-system)
    (home-page "https://github.com/tarides/ocaml-eglot")
    (synopsis "An OCaml companion for Eglot")
    (description
     "This package provides a development environment for writing OCaml code.  Built
on top of `ocaml-lsp-server` (https://ocaml.org/p/ocaml-lsp-server/latest) via
Eglot (https://www.gnu.org/software/emacs/manual/html_mono/eglot.html) for LSP
interactions. `ocaml-eglot` provides standard implementations of the various
custom-requests exposed by `ocaml-lsp-server`.  Under the bonnet, `ocaml-eglot`
and `ocaml-lsp-server` take advantage of `merlin`
(https://ocaml.org/p/merlin-lib/latest) as a library to provide advanced IDE
services.")
    (license #f)))
