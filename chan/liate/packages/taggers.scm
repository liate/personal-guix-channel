(define-module (liate packages taggers)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system python)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix l:)
  #:use-module (guix gexp)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages mp3)
  #:use-module (gnu packages music)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages textutils)
  #:use-module (gnu packages video)
  )

(define-public puddletag
  (package
    (name "puddletag")
    (version "2.2.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/puddletag/puddletag")
                    (commit version)))
              (sha256 (base32 "1y0zii1bdsai7j3qns4054lqwqvrwwibir7x1fcyvxl8wnj5z899"))
              (snippet
               #~(begin
                   (use-modules ((guix build utils)))
                   ;; Upstream broke some symlinks on a file reorg, never fixed
                   (define (replace-link file actual-source)
                     (delete-file file)
                     (copy-file actual-source file))
                   (replace-link "docsrc/changelog" "changelog")
                   (replace-link "README" "README.md")))))
    (build-system python-build-system)
    (arguments
     (list #:tests? #f
           #:phases #~(modify-phases %standard-phases
                        (delete 'sanity-check))))
    (inputs
     (list python-pyqt
           python-configobj
           python-pyparsing
           python-mutagen
           python-unidecode
           python-pyacoustid
           python-urllib3))
    (synopsis "Powerful, simple, audio tag editor for GNU/Linux")
    (description
     "puddletag is an audio tag editor (primarily created) for GNU/Linux similar to the Windows
program, Mp3tag. Unlike most taggers for GNU/Linux, it uses a spreadsheet-like layout so that
all the tags you want to edit by hand are visible and easily editable.

The usual tag editor features are supported like extracting tag information from filenames,
renaming files based on their tags by using patterns and basic tag editing.

Then there’re Functions, which can do things like replace text, trim it, do case conversions,
etc. Actions can automate repetitive tasks. Doing web lookups using Amazon (including cover
art), Discogs (does cover art too!), FreeDB and MusicBrainz is also supported. There’s quite a
bit more, but I’ve reached my comma quota.

Supported formats: ID3v1, ID3v2 (mp3), MP4 (mp4, m4a, etc.), VorbisComments (ogg, flac),
Musepack (mpc), Monkey’s Audio (.ape) and WavPack (wv).")
    (home-page "https://docs.puddletag.net/index.html")
    (license l:gpl3+)))

(define qoobar
  (package
    (name "qoobar")
    (version "1.7.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/alexnovichkov/qoobar.git")
             (commit (string-append "v" version))))
       (sha256
        (base32 "0b78hj6zmqsb62wf5zbwfwg9bzy4q8c1lzihdmv76gpdzwhsadrh"))))
    (build-system gnu-build-system)
    (arguments
     (list
      #:make-flags
      #~(list (string-append "INSTALL_ROOT="
                             #$output))
      #:phases
      #~(modify-phases %standard-phases
          (add-before 'configure 'fix-qmake-file
            (lambda* _
              (substitute* (list "src/qoobar_app/qoobar_app.pro")
                (("^INSTALL_ROOT = /") ""))
              )
            )
          (replace 'configure
            (lambda* (#:key make-flags #:allow-other-keys)
              (apply invoke "qmake"
                     make-flags))))))
    (inputs
     (list qtbase
           qt5compat
           qtdeclarative
           qtcolorwidgets
           ;; …other qt
           ffmpeg
           ;; libswresample
           libebur128
           taglib
           libdiscid
           enca
           zlib
           shntool))
    (native-inputs
     (list qtbase                       ; qmake
           qttools                      ; lupdate
           pkg-config))
    (home-page "https://qoobar.sourceforge.io/en/index.htm")
    (synopsis "A free tag editor for classical music")
    (description
     "Qoobar is a simple tagger which is designed for editing tags in files of classical music.

Qoobar uses Qt for GUI and Taglib for tags manipulation and runs under GNU/Linux, Mac OS X and
Windows.

Qoobar also uses parts of other libraries: loudgain library, some files from Qt Creator sources
and KDE sources.")
    (license l:gpl3+)))
