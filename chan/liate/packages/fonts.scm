(define-module (liate packages fonts)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system font)
  #:use-module (guix build-system copy)
  #:use-module (gnu packages compression))

(define-public font-scientifica
  (package
    (name "font-scientifica")
    (version "2.3")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "https://github.com/nerdypepper/scientifica/releases/download/"
                    "v" version "/scientifica.tar"))
              (sha256
               (base32
                "0zwa3s75lvbky2vn73i1fmxa37hi3zfm7f6wfpqwcip8l1lpi1gh"))))
    (build-system copy-build-system)
    (arguments
     `(#:install-plan
       '(("ttf/" "share/fonts/truetype/")
         ("bdf/" "share/fonts/bdf/"))))
    (home-page "https://github.com/NerdyPepper/scientifica")
    (synopsis "tall, condensed, bitmap font for geeks")
    (description "scientifica is largely based on creep, with a number of
minor tweaks to improve readability (a matter of taste of course). Most
characters are just 4px wide, which is brilliant for low dpi(90-120) displays.")
    (license license:silofl1.1)))

(define-public font-merriweather
  ;; The changelog says there's a version 2.100, but there's no tag/release for it,
  ;; and I don't feel like hunting down the commit for it
  (let ((last-version "2.100")
        (commit "4481226"))
    (package
     (name "font-merriweather")
     (version (git-version last-version "0" commit))
     (source (origin
               (method git-fetch)
               (uri (git-reference
                     (url "https://github.com/SorkinType/Merriweather/")
                     (commit commit)))
               (sha256
                (base32
                 "0nxkfcacz1yz9ybldn14jhyr1z7xszg11p91lq62vn6c7dwhhkj1"))))
     (build-system copy-build-system)
     (arguments
      `(#:install-plan
        '(("fonts/otf/" "share/fonts/opentype/")
          ("fonts/ttf/" "share/fonts/truetype/")
          ("fonts/webfonts/" "share/fonts/web/"))))
     (home-page "https://github.com/SorkinType/Merriweather/")
     (synopsis "a serif typeface")
     (description
      "Merriweather offers a Renaissance warmth while using proportions which are
space-saving. It is suitable for editorial design, news and other kinds of space sensitive
typography. Merriweather is an evolving project and will be updated.

Merriweather is a Unicode typeface family that supports languages that use the Latin and
Cyrillic scripts and its variants, and could be expanded to support other scripts e.g. Greek,
Devanagari, Ethiopic, Arabic and so on.")
     (license license:silofl1.1))))

(define-public font-departure-mono
  (let ((version "1.346"))
    (package
      (name "font-departure-mono")
      (version version)
      (source
       (origin
         (method url-fetch)
         (uri (string-append
               "https://departuremono.com/assets/DepartureMono-"
               version
               ".zip"))
         (sha256
          (base32
           "0p5vz60sw51882y0fg6sg06hl0plcyb867q3ykny7c7r5g0xlj0y"))))
      (build-system copy-build-system)
      (arguments
       `(#:install-plan
         '(("./" "share/fonts/opentype/" #:include-regexp (".*\\.otf"))
           ("./" "share/fonts/web/" #:include-regexp (".*\\.woff")))))
      (inputs (list unzip))
      (home-page "https://departuremono.com/")
      (synopsis "a monospaced pixel font with a lo-fi technical vibe")
      (description
       "Departure Mono is a monospaced pixel font with a lo-fi technical vibe")
      (license license:silofl1.1))))
