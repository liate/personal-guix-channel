(define-module (liate packages wrappers)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix build utils)
  #:use-module (gnu packages ocaml)
  #:use-module (gnu packages rsync)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages haskell-apps)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages base)
  #:use-module (guix build-system copy))

;; TODO: make not install to OCAML_PATH
(define-public opam-cur-wrapped
  (package
    (inherit opam)
    (name "opam-wrapped")
    (version "2.2.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/ocaml/opam")
                    (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1f9wkp5dq29qfxc5nlck8pp8r61siaa9hhjxzy67lmx3d6i54052"))))
    (native-inputs
     (list dune
           ocaml-cppo
           git
           rsync
           mercurial
           darcs
           gcc-toolchain
           gnu-make))
    (arguments
     (list #:tests? #f ;; That's most of the configuration of opam, and this is just a wrapper
           #:package "opam"
           #:phases
           #~(modify-phases %standard-phases
               (add-after 'install 'wrap
                 (lambda _
                   (define (search-paths under)
                     (map (lambda (input) (string-append input under))
                          '(#$git
                            #$rsync
                            #$mercurial
                            #$darcs
                            #$gcc-toolchain
                            #$gnu-make)))
                   (wrap-program (string-append #$output "/bin/opam")
                     `("PATH" ":" suffix
                       ,(search-paths "/bin/"))
                     `("LIBRARY_PATH" ":" suffix
                       (,(string-append #$gcc-toolchain "/lib/")))
                     `("C_INCLUDE_PATH" ":" suffix
                       (,(string-append #$gcc-toolchain "/include/")))))))))))
