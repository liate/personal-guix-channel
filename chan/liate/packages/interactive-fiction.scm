(define-module (liate packages interactive-fiction)
  #:use-module (guix packages)
  #:use-module ((guix licenses) #:prefix l:)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages image)
  #:use-module (gnu packages mp3)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages speech)
  #:use-module (ice-9 string-fun))

(define-public if-dialog
  (let ((compiler-version "0m03") ; output of dialogc --version, without the #\/
        (stdlib-version "0.46"))
    (package
      (name "if-dialog")
      (version (string-append compiler-version "-" stdlib-version))
      (source
       (origin
         (method url-fetch)
         (uri (string-append "https://hd0.linusakesson.net/files/dialog-" ; 0m03_0_46
                             compiler-version "_"
                             (string-replace-substring stdlib-version "." "_")
                             ".zip"))
         (sha256 (base32 "0bvyd6983frlak19pivwbchzhc0zzikikn7s8lm3gxi57raq21zx"))))
      (build-system gnu-build-system)
      (arguments 
       `(#:phases
         (modify-phases %standard-phases
           (add-before 'configure 'cd-to-source-dir
             (lambda* _
               (chdir "src")))
           (delete 'configure)
           (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (let ((bin (string-append (assoc-ref outputs "out")
                                         "/bin"))
                     (include-dir (string-append (assoc-ref outputs "out")
                                                 "/share/dialog/"))
                     (doc-dir (string-append (assoc-ref outputs "out")
                                          "/share/doc/dialog/")))
                 (install-file "dialogc" bin)
                 (install-file "dgdebug" bin)
                 (install-file "../stdlib.dg" include-dir)
                 (install-file "../stddebug.dg" include-dir)
                 (copy-recursively "../docs/" doc-dir)
                 #t))))
         #:tests? #f))
      (native-inputs (list unzip))
      (native-search-paths
       (list (search-path-specification
              (variable "DIALOG_STDLIB")
              (separator #f)
              (files '("share/dialog/")))))
      (home-page "https://linusakesson.net/dialog/")
      (synopsis "A domain-specific language for creating works of interactive fiction")
      (description
       "Dialog is a domain-specific language for creating works of
interactive fiction. It is heavily inspired by Inform 7 (Graham Nelson
et al. 2006) and Prolog (Alain Colmerauer et al. 1972).

An optimizing compiler, dialogc, translates high-level Dialog code into
Z-code, a platform-independent runtime format originally created by
Infocom in 1979.

Since Dialog version 0d/01 there's an interactive debugger, and since
version 0g/01 there's a separate backend for the Å-machine story
format.")
      (license l:bsd-2))))

(define-public gargoyle
  (package
    (name "gargoyle")
    (version "2022.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/garglk/garglk")
                    (commit version)))
              (sha256 (base32 "0nyn4l8sdgxmf7jxk8l6gii6jsvdh0nfx7f46mm8zhhzwzzvav1k"))
              (file-name (git-file-name name version))))
    (build-system cmake-build-system)
    (arguments
     `(#:configure-flags
       (list "-DSOUND=QT" "-DWITH_TTS=ON")
       #:tests? #f))
    (native-inputs (list pkg-config))
    (inputs (list qtbase-5 fontconfig freetype libjpeg-turbo libpng zlib
                  speech-dispatcher glib ;; for speech synthesis
                  qtmultimedia-5 libsndfile mpg123 libopenmpt)) ;; for other audio
    (synopsis " An interactive fiction player")
    (home-page "https://ccxvii.net/gargoyle/")
    (description
     "Gargoyle is an IF player that supports all the major interactive
fiction formats.

Most interactive fiction is distributed as portable game files. These
portable game files come in many formats. In the past, you used to have
to download a separate player (interpreter) for each format of IF you
wanted to play.

Gargoyle is based on the standard interpreters for the formats it
supports. Gargoyle is free software released under the terms of the GNU
General Public License.")
    (license (list l:gpl3+ l:bsd-2 l:artistic2.0 l:expat l:bsd-3 l:silofl1.1))))
