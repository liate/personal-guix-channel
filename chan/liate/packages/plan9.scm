(define-module (liate packages plan9)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build utils)
  #:use-module ((guix licenses) #:prefix l:)
  #:use-module (gnu packages)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages fontutils)
  #:use-module ((gnu packages base) #:prefix b:))

;; TODO: remove BigelowHolmes-licensed fonts, put in (gnu packages plan9)
(define-public plan9port
  (let ((commit "a6ad39aaaa36b8aadc5c35bfc803afbde32918c0"))
    (package
      (name "plan9port")
      (version "20200526")
      (source (origin
                (method url-fetch)
                (uri (string-append
                      "https://github.com/9fans/plan9port/archive/" commit
                      ".tar.gz"))
                (sha256
                 (base32
                  "11iagjsd3r5w4qi74ymczfm7klrqyrw0a1xvqpylmg0adhzh6172"))
                (file-name "plan9port.tar.gz")))
      (build-system gnu-build-system)
      (arguments
       `(#:tests? #f
         #:phases
         (modify-phases %standard-phases
           (replace 'unpack
             (lambda* (#:key source outputs #:allow-other-keys)
               (let ((out (assoc-ref outputs "out")))
                 (begin
                   (mkdir out)
                   (chdir out)
                   (invoke "tar" "xvf" source)
                   (rename-file ,(string-append "plan9port-" commit) "plan9")
                   (chdir "./plan9"))) #t))
           (delete 'bootstrap)
           (replace 'configure
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((plan9 (string-append (assoc-ref outputs "out") "/plan9"))
                      (conf (string-append plan9 "/LOCAL.config"))
                      (install (string-append plan9 "/INSTALL")))
                 ;; Adapted from gentoo's plan9port ebuild for this version
                 (substitute* install
                   (("/bin:/usr/bin:") "")
                   (("mk -k") "mk"))
                 (call-with-output-file conf
                   (lambda (s)
                     ;; Adapted from https://github.com/9fans/plan9port/issues/61#issuecomment-261048348
                     (format s "WSYSTYPE=x11~%FONTSRV=fontsrv~%")
                     (format s "CFLAGS=$CFLAGS' -I~a/include/freetype2"
                             (assoc-ref inputs "freetype"))
                     (for-each (lambda (input)
                                 (format s " -I~a/include/X11"
                                         (assoc-ref inputs input)))
                               '("libx11" "libxext" "libxt" "xorgproto"
                                 "libxau" "libxmu"))
                     (format s "'~%"))))))
           (replace 'build
             (lambda _
               (invoke "./INSTALL")))
           (delete 'install)
           (delete 'strip))))
      (inputs `(("libx11" ,libx11) ("libxext" ,libxext)
                ("libxt" ,libxt)
                ("xorgproto" ,xorgproto)
                ("libxau" ,libxau)
                ("libxmu" ,libxmu)
                ("freetype" ,freetype)
                ("fontconfig" ,fontconfig)
                ("gcc" ,gcc-toolchain)
                ("which" ,b:which)
                ("perl" ,perl)))
      (native-search-paths
       (list (search-path-specification
              (variable "PLAN9")
              (separator #f)
              (files '("plan9")))))
      (synopsis "A port of many Plan 9 libraries and programs to Unix")
      (description
       "Plan 9 from User Space (aka plan9port) is a port of many Plan 9 programs from
their native Plan 9 environment to Unix-like operating systems.

Add @env{$PLAN9/bin} to the end of your path to use.")
      (home-page "https://9fans.github.io/plan9port/")
      ;; Gentoo has LICENSE="9base BSD-4 MIT LGPL-2.1 BigelowHolmes"
      (license (list l:expat l:bsd-4 l:lgpl2.1))))) ;; TODO: get the MIT license in (guix licenses)
