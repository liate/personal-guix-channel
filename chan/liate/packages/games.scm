(define-module (liate packages games)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix l:)
  #:use-module (gnu packages)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages image)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages video)
  #:use-module (gnu packages xiph)
  #:use-module (gnu packages xml)
  ;; For kandria
  #:use-module (nonguix build-system binary)
  #:use-module (nonguix licenses)
  #:use-module (gnu packages lisp)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages base)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages xorg)
  ;; For working fortunes
  #:use-module (gnu packages toys)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  )

(define-public aleph-one
  (let ((version-date "20230119"))
    (package
      (name "aleph-one")
      (version "1.6.1")
      (source (origin
                (method url-fetch)
                (uri (string-append
                      "https://github.com/Aleph-One-Marathon/alephone/releases/download/release-"
                      version-date "/AlephOne-" version-date ".tar.bz2"))
                (sha256
                 (base32
                  "09ihbjm5synyhl8gh5ydh4kvx8rxyil6h2bb52iw55i8cmym2bxc"))))
      (build-system gnu-build-system)
      (arguments
       (list
        #:configure-flags
        #~(list (string-append "--with-boost-libdir="
                               #$(file-append boost "/lib")))))
      (native-inputs (list pkg-config))
      (inputs (list boost
                    curl
                    expat
                    ffmpeg
                    glu
                    libglvnd
                    libpng
                    (sdl-union (list sdl2 sdl2-ttf sdl2-image sdl2-net
                                     sdl2-mixer))
                    speex
                    speexdsp
                    zziplib))
      (synopsis "An open-source version of Marathon 2's game engine")
      (description
       "Aleph One is the open source continuation of Bungie's Marathon 2 FPS game
engine. Aleph One plays Marathon, Marathon 2, Marathon Infinity, and 3rd-party
content on a wide array of platforms, with (optional) OpenGL rendering, Internet
play, Lua scripting, and more.")
      (home-page "https://alephone.lhowon.org/")
      (license l:gpl3+))))

(define-public kandria-demo
  (let ((ld-library-path-libs (list zlib openssl mesa glibc curl pulseaudio libevdev)))
    (package
      (name "kandria-demo")
      (version "0.2.6")
      (source
       (origin
         ;; Since game-local-fetch annoyingly doesn't have a /zipbomb equivalent
         (method url-fetch/zipbomb)
         (uri (string-append (or (getenv "GUIX_GAMING_PATH")
                                 "~/.guix-game-sources")
                             "/" "kandria-windows-linux.zip"))
         (sha256
          (base32
           "128fcyp6b4wblaf064bx09qkwzcwxaijcdfxlbkbbk56mqrp9113"))))
      (supported-systems '("x86_64-linux"))
      (build-system binary-build-system)
      (inputs ld-library-path-libs)
      (native-inputs
       (list unzip))
      (arguments
       `(#:install-plan
         '(("." "share/kandria/"))
         #:phases
         (modify-phases %standard-phases
           (add-after 'install 'create-wrapper
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (bin (string-append output "/bin/"))
                      (wrapper (string-append bin "kandria-demo"))
                      (real (string-append output
                                           "/share/kandria/kandria-linux.run"))
                      (shell (string-append (assoc-ref inputs "bash") "/bin/bash"))
                      (ld-linux (string-append (assoc-ref inputs "glibc")
                                               "/lib/ld-linux-x86-64.so.2"))
                      (library-paths
                       (string-join
                        (map (lambda (input)
                               (string-append (assoc-ref inputs input) "/lib/"))
                             ',(map package-name ld-library-path-libs))
                        ":")))
                 (mkdir-p bin)
                 (call-with-output-file wrapper
                   (lambda (file)
                     (format file
                             (string-append
                              "#!~a~%"
                              "LD_LIBRARY_PATH=~a TRIAL_LOGFILE=~~/.log/trial.log"
                              " exec ~a ~a --core ~:*~a $*~%")
                             shell
                             library-paths ld-linux real)))
                 (chmod wrapper #o555)))))))
      (home-page "https://kandria.com/")
      (synopsis "A 2D action game written in Common Lisp")
      (description
       "Kandria is a hand-crafted 2D action game, which combines the wall-climbing
verticality and puzzle platforming of Celeste, with the tight hack and slash
swordplay of NieR:Automata, all set within an epic, post-apocalyptic RPG world.
Venture far and wide, determine who is truly friend or foe, and recruit what
allies you can to help you survive.")
      (license (undistributable "No License")))))

(define-public generic-fortunes-jkirchartz
  ;; Since the packagers decided to make things only work with the incomplete daikichi
  (package
    (inherit fortunes-jkirchartz)
    (name "generic-fortunes-jkirchartz")
    (arguments
     (substitute-keyword-arguments (package-arguments fortunes-jkirchartz)
       ((#:install-plan _)
        #~`(("." "share/fortunes" #:exclude-regexp ("README" "Makefile" "bin" "LICENSE"))))))))
