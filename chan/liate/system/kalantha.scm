(define-module (liate system kalantha)
  #:use-module (gnu)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages wm)
  #:use-module (gnu services audio)
  #:use-module (gnu services cups)
  #:use-module (gnu services mcron)
  #:use-module (gnu services networking)
  #:use-module (gnu services docker)
  #:use-module (gnu services sysctl)
  #:use-module (gnu system)
  #:use-module (gnu system setuid)
  #:use-module (nongnu packages linux)
  #:use-module (nongnu packages firmware)
  #:use-module (nongnu system linux-initrd)

  #:use-module (liate system common))
(use-service-modules audio cups dbus desktop dns docker linux mcron networking networking pm
                     sddm shepherd sound ssh sysctl sysctl virtualization xorg)
(use-package-modules admin base bootloaders certs cups cryptsetup display-managers
                     emacs firmware gnome linux polkit samba shells terminals wm)

;;; cron jobs
(define updatedb-job
  #~(job '(next-hour '(3))
         ;; Update locate's database, ignoring some directories, without messing up other
         ;; processes' io too much
         (format #f "~a/bin/ionice -c 3 ~a/bin/updatedb --prunepaths='/gentoo /snapshots /sys /proc /tmp /run /dev /mnt /gnu /guix'"
                 #$(specification->package "util-linux")
                 #$(specification->package "findutils"))))

(define (btrfs-fs-options subvol)
  (string-append "compress=zstd,subvol=" subvol))

(define kalantha
  (operating-system
   (inherit template)

    ;; system-specific stuff
    (host-name "kalantha")
   (bootloader
    (bootloader-configuration
     (bootloader grub-efi-bootloader)
     (targets '("/boot/efi"))
     (keyboard-layout (operating-system-keyboard-layout template))))
   (file-systems
    (cons* (file-system
            (mount-point "/boot/efi")
            (device (uuid "8676-C973" 'fat32))
            (type "vfat"))
           (file-system
            (mount-point "/")
            (device (file-system-label "btrfs-pool"))
            (type "btrfs")
            (options (btrfs-fs-options "@guix")))
           (file-system
            (mount-point "/gnu")
            (device (file-system-label "btrfs-pool"))
            (type "btrfs")
            (options (btrfs-fs-options "@gnu")))
           (file-system
            (mount-point "/home")
            (device (file-system-label "btrfs-pool"))
            (type "btrfs")
            (options (btrfs-fs-options "@home")))
           (file-system
            (mount-point "/mnt/btrfs-root")
            (device (file-system-label "btrfs-pool"))
            (type "btrfs")
            (options (btrfs-fs-options "/")))
           %base-file-systems))

    ;; nonguix stuff
    (kernel linux)
    (initrd microcode-initrd)
    (firmware (cons* linux-firmware %base-firmware))

    ;; Packages and services
    (packages
     (cons* sway nss-certs zsh kitty abstractdark-sddm-theme
            cups
            linux-pam cifs-utils
            polkit geoclue fwupd-nonfree
            %base-packages))
    (setuid-programs
     (cons* (setuid-program
             (program (file-append (specification->package "inetutils")
                                   "/bin/traceroute")))
            (setuid-program
             (program (file-append (specification->package "cifs-utils")
                                   "/sbin/mount.cifs")))
            %setuid-programs))
    (services
     (cons* (simple-service 'extra-hosts
                            hosts-service-type
                            (list (host "8.8.8.8" "test")))
            (service screen-locker-service-type
                     (screen-locker-configuration
                      (name "swaylock")
                      (program (file-append swaylock "/bin/swaylock"))
                      (using-setuid? #f)))
           (service sddm-service-type
                    (sddm-configuration
                     (display-server "wayland")
                     (theme "abstractdark")))

            (service openssh-service-type)

            (service docker-service-type)
            (service qemu-binfmt-service-type
                     (qemu-binfmt-configuration
                      (platforms (lookup-qemu-platforms "arm" "aarch64"))))
            (service tlp-service-type)
            (modify-services %desktop-services
              (delete gdm-service-type)
              (guix-service-type
               config
               => (guix-configuration
                   (inherit config)
                   (substitute-urls
                    (cons* "https://substitutes.nonguix.org"
                           "https://guix.bordeaux.inria.fr"
                           %default-substitute-urls))
                   (authorized-keys
                    (cons* (local-file "./files/nonguix-substitutes-key.pub")
                           (local-file "./files/guix-past-substitutes-key.pub")
                           %default-authorized-guix-keys))))
              (sysctl-service-type
               config => (sysctl-configuration
                          (settings (cons* '("kernel.core_pattern"
                                             . "/tmp/cores/core.%e.%p.%h.%t")
                                           %default-sysctl-settings)))))))
    (name-service-switch %mdns-host-lookup-nss)))
kalantha
