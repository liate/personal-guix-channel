(define-module (liate system pbpro-installer)
  #:use-module (gnu)
  #:use-module (gnu system)
  #:use-module (gnu system install)
  #:use-module (gnu bootloader)
  #:use-module (gnu bootloader u-boot)
  #:use-module (nongnu packages linux)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages firmware)
  #:use-module (gnu packages cryptsetup)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages disk)
  #:use-module (gnu packages certs)
  #:use-module (gnu packages ssh)
  #:use-module (gnu system images pinebook-pro)
  #:use-module (gnu image)
  #:use-module (guix platforms arm)
  #:use-module (gnu services networking)
  #:use-module (gnu services base)
  ;; For wifi firmware
  #:use-module (liate packages firmware)
  #:use-module (nongnu packages firmware)
  )

(define-public pbpro-installation-os
  (operating-system
    (inherit pinebook-pro-barebones-os)
    (bootloader (bootloader-configuration
                 (bootloader grub-efi-removable-bootloader)
                 (targets '("/boot/efi"))))
    (packages (cons* network-manager wpa-supplicant
                     cryptsetup btrfs-progs parted mtd-utils
                     nss-certs openssh-sans-x
                     (operating-system-packages pinebook-pro-barebones-os)))
    (file-systems (cons* (file-system
                           (device (uuid "DEE6-E377" 'fat16))
                           (mount-point "/mnt/installer/boot/efi")
                           (type "vfat"))
                         (file-system
                           (device (file-system-label "installer-root"))
                           (mount-point "/")
                           (type "ext4"))
                         %base-file-systems))
    (services (cons* (service wpa-supplicant-service-type
                              (wpa-supplicant-configuration
                               (config-file
                                (plain-file "wpa_supplicant.conf"
                                            (string-join
                                             '("ctrl_interface=/run/wpa_supplicant"
                                               "update_config=1")
                                             "\n" 'suffix)))))
                     (service network-manager-service-type)
                     ((@@ (gnu system install) cow-store-service))
                     (modify-services %base-services
                       (special-files-service-type
                        config => (append
                                   `(("/etc/guix-config" ,(local-file "/home/liate/guix-config"
                                                                      #:recursive? #t))
                                     ("/etc/full-channels.scm"
                                      ,(local-file "/home/liate/.config/guix/channels.scm")))
                                   config)))))
    
    (kernel linux-arm64-generic)
    (timezone "America/New_York")
    (firmware (cons* pinebook-pro-wifi-firmware broadcom-bt-firmware bluez-firmware
                     %base-firmware))))

#;(define-public pbpro-installer-raw-image
  (image
   (inherit
    (os+platform->image pbpro-installation-os aarch64-linux
                        #:type pinebook-pro-image-type))
   (name 'pbpro-installer-raw-image)))

pbpro-installation-os
