(define-module (liate system common)
  #:use-module (gnu)
  #:use-module (gnu system)
  #:use-module (guix gexp)
  #:export (template
            liate
            btrbk-snapshots-job
            btrfs-balance-job))
(use-package-modules backup linux shells)

(define liate
  (user-account
   (name "liate")
   (comment "Andrew Patterson")
   (group "users")
   (home-directory "/home/liate")
   (shell (file-append zsh "/bin/zsh"))
   (supplementary-groups
    '("audio" "docker" "input" "kvm" "lp" "netdev" "power" "tty" "video" "wheel"))))

(define (btrbk-snapshots-job config)
  #~(job '(next-minute '(0))
         (format #f "~a/bin/btrbk -qc ~a run"
                 #$btrbk
                 #$config)))

(define (btrfs-balance-job root)
  #~(job '(next-day '(1))
         (format #f "~a/bin/btrfs balance start -musage=50 -dusage=50 ~a"
                 #$btrfs-progs
                 #$root)))

(define template
  (operating-system
    (locale "en_US.utf8")
    (timezone "America/New_York")
    (keyboard-layout (keyboard-layout "us" #:options '("compose:ralt" "ctrl:swapcaps")))
    (host-name "template")

    (users (cons* liate
                  %base-user-accounts))
    (groups (cons* (user-group
                    (name "power"))
                   %base-groups))

    (file-systems #f)
    (bootloader #f)))

