(define-module (liate system nostalgia-for-inf-0)
  #:use-module (gnu)
  #:use-module (gnu bootloader)
  #:use-module (gnu packages)
  #:use-module (gnu system)
  #:use-module (gnu system setuid)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (nongnu packages linux)
  #:use-module (nongnu packages firmware)
  #:use-module (nongnu system linux-initrd)

  #:use-module (liate system common))
(use-service-modules audio cups dbus desktop dns docker linux mcron networking networking
                     nix pm sddm shepherd sound ssh syncthing sysctl sysctl virtualization xorg)
(use-package-modules admin base bootloaders certs cups cryptsetup display-managers
                     emacs firmware gnome linux package-management polkit samba shells
                     terminals wm)

(define (btrfs-fs-options subvol)
  (string-append "compress=zstd,subvol=" subvol))

(define nostalgia-for-inf-0
  (operating-system
   (host-name "nostalgia-for-inf-0")

   (locale "en_US.utf8")
   (timezone "America/New_York")
   (keyboard-layout (keyboard-layout "us" #:options '("compose:ralt" "ctrl:swapcaps")))

   (users (cons* liate
                 %base-user-accounts))
   (groups (cons* (user-group
                   (name "power"))
                  %base-groups))

   (kernel linux)
   (initrd microcode-initrd)
   (firmware (cons* linux-firmware %base-firmware))

   (bootloader
    (bootloader-configuration
     (bootloader grub-efi-bootloader)
     (targets '("/boot/efi"))
     (keyboard-layout keyboard-layout)))
   (mapped-devices
    (list
     (mapped-device
      ;; TODO: find out why it wasn't/isn't working with the LUKS UUID
      (source "/dev/sda2")
      ;; (source (uuid "535b9843-f639-453e-8856-fdafce731f42"))
      (target "root")
      (type luks-device-mapping))))
   (file-systems
    (cons* (file-system
            (mount-point "/boot/efi")
            (device (uuid "A3EA-B3A1" 'fat32))
            (type "vfat"))
           (file-system
            (dependencies mapped-devices)
            (mount-point "/")
            (device "/dev/mapper/root")
            (type "btrfs")
            (options (btrfs-fs-options "@guix")))
           (file-system
            (dependencies mapped-devices)
            (mount-point "/gnu")
            (device "/dev/mapper/root")
            (type "btrfs")
            (options (btrfs-fs-options "@gnu")))
           (file-system
            (dependencies mapped-devices)
            (mount-point "/home")
            (device "/dev/mapper/root")
            (type "btrfs")
            (options (btrfs-fs-options "@home")))
           (file-system
            (dependencies mapped-devices)
            (mount-point "/mnt/btrfs-root")
            (device "/dev/mapper/root")
            (type "btrfs")
            (options (btrfs-fs-options "/")))
           (file-system
            (mount-point "/mnt/nfs/lucrehulk-data/")
            (mount? #f)
            (device "lucrehulk.lan:/data")
            (options "nolock,user,rw,noauto,noatime,nodiratime,exec")
            (type "nfs"))
           %base-file-systems))
   (swap-devices
    (list (swap-space
           (target "/mnt/btrfs-root/@swaps/swapfile")
           (dependencies (filter (file-system-mount-point-predicate "/mnt/btrfs-root")
                                 file-systems)))))

   (packages
    (cons* sway zsh kitty abstractdark-sddm-theme
           cups
           nix
           linux-pam cifs-utils
           polkit geoclue bolt fwupd-nonfree
           %base-packages))
   (setuid-programs
    (cons* (setuid-program
            (program (file-append inetutils
                                  "/bin/traceroute")))
           (setuid-program
            (program (file-append cifs-utils
                                  "/sbin/mount.cifs")))
           %setuid-programs))

   (services
    (cons* (simple-service 'extra-hosts
                           hosts-service-type
                           (list (host "8.8.8.8" "test")))
           (simple-service 'containers-policy
                           etc-service-type
                    (list `("containers/policy.json"
                            ,(plain-file "policy.json"
                                         "{
    \"default\": [
        {
            \"type\": \"insecureAcceptAnything\"
        }
    ],
    \"transports\":
        {
            \"docker-daemon\":
                {
                    \"\": [{\"type\":\"insecureAcceptAnything\"}]
                }
        }
}"))))
           (simple-service 'btrfs-cron-jobs
                           mcron-service-type
                           (list (btrbk-snapshots-job
                                  (local-file "./files/nostalgia-for-inf.0-btrbk.conf"))
                                 (btrfs-balance-job "/mnt/btrfs-root")))

           (service nix-service-type)
           (service syncthing-service-type
                    (syncthing-configuration
                     (user "liate")))
           (service docker-service-type)
           (service containerd-service-type)

           (service screen-locker-service-type
                    (screen-locker-configuration
                     (name "swaylock")
                     (program (file-append swaylock "/bin/swaylock"))
                     (using-setuid? #f)))
           (service sddm-service-type
                    (sddm-configuration
                     #;(display-server "wayland")
                     (theme "abstractdark")))
           (service openssh-service-type)
           (service qemu-binfmt-service-type
                    (qemu-binfmt-configuration
                     (platforms (lookup-qemu-platforms "arm" "aarch64"))))
           (service cups-service-type
                    (cups-configuration
                     (web-interface? #t)))
           (service tlp-service-type)
           (service mpd-service-type
                    (mpd-configuration
                     (user liate)
                     (music-directory "/home/liate/music/")
                     (state-file "/home/liate/.mpd/state")
                     (playlist-directory "/home/liate/.mpd/playlists/")
                     (outputs
                      (list (mpd-output
                             (type "pipewire"))))
                     (environment-variables
                      ;; TODO: make based on the MPD user's uid?
                      (list (string-append "XDG_RUNTIME_DIR="
                                           "/run/user/1000/")
                            "PULSE_CLIENTCONFIG=/etc/pulse/client.conf"
                            "PULSE_CONFIG=/etc/pulse/daemon.conf"))))
           (service bluetooth-service-type)
           ;; (service ipfs-service-type)
           (service tor-service-type)
           (modify-services %desktop-services
                            (delete gdm-service-type)
                            (geoclue-service-type
                             config
                             => (geoclue-configuration
                                 (inherit config)
                                 (applications
                                  (cons* (geoclue-application "firefox")
                                         (geoclue-application "redshift")
                                         (geoclue-application "emacs")
                                         (geoclue-application "geoclue-where-am-i")
                                         %standard-geoclue-applications))))
                            (dbus-root-service-type
                             config
                             => (dbus-configuration
                                 (inherit config)
                                 (services
                                  (list bolt fwupd-nonfree))))
                            (polkit-service-type
                             config
                             => (polkit-configuration
                                 (inherit config)
                                 (actions (list bolt))))
                            (guix-service-type
                             config
                             => (guix-configuration
                                 (inherit config)
                                 (substitute-urls
                                  (cons* #;"http://lucrehulk.lan:8181"
                                   "https://cuirass.genenetwork.org"
                                   "https://substitutes.nonguix.org"
                                   "https://guix.bordeaux.inria.fr"
                                   %default-substitute-urls))
                                 (authorized-keys
                                  (cons* (local-file "./files/nonguix-substitutes-key.pub")
                                         (local-file "./files/lucrehulk-substitutes-key.pub")
                                         (local-file "./files/guix-past-substitutes-key.pub")
                                         (local-file "./files/genenetwork-substitutes-key.pub")
                                         %default-authorized-guix-keys))))
                            (sysctl-service-type
                             config
                             => (sysctl-configuration
                                 (settings (cons* '("kernel.core_pattern"
                                                    . "/tmp/cores/core.%e.%p.%h.%t")
                                                  %default-sysctl-settings)))))))
   (name-service-switch %mdns-host-lookup-nss)))
nostalgia-for-inf-0
