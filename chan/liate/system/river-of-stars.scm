(define-module (liate system river-of-stars)
  #:use-module (gnu)
  #:use-module (gnu bootloader)
  #:use-module (gnu packages)
  #:use-module (gnu system)
  #:use-module (gnu system setuid)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (ice-9 format)
  #:use-module (ice-9 textual-ports)
  #:use-module (liate packages firmware)
  #:use-module (liate packages hare)
  #:use-module (liate packages linux)
  #:use-module (nongnu packages linux)
  #:use-module (nongnu packages firmware)
  )
(use-service-modules desktop dns networking ssh sddm sound xorg linux sysctl shepherd)
(use-package-modules linux admin firmware cryptsetup shells wm emacs certs terminals
                     base bootloaders)

(define river-of-stars
  (operating-system
    (host-name "river-of-stars")

    (locale "en_US.utf8")
    (timezone "America/New_York")
    (keyboard-layout
     (keyboard-layout "us" #:options '("compose:ralt" "ctrl:swapcaps")))
    (users (cons* (user-account
                   (name "liate")
                   (comment "Andrew Patterson")
                   (group "users")
                   (home-directory "/home/liate")
                   (shell (file-append zsh "/bin/zsh"))
                   (supplementary-groups
                    '("audio" "input" "kvm" "lp" "netdev" "power" "seat" "tty" "video" "wheel")))
                  %base-user-accounts))
    (groups (cons* (user-group
                    (name "power"))
                   %base-groups))

    (kernel linux-arm64-generic)
    (firmware (cons* pinebook-pro-wifi-firmware broadcom-bt-firmware bluez-firmware
                     ap6256-firmware
                     %base-firmware))
    (initrd-modules '("dm-crypt" "xts" "virtio_pci" "virtio_balloon" "virtio_blk" "virtio_net"))

    (bootloader (bootloader-configuration
                 (bootloader grub-efi-removable-bootloader)
                 (targets '("/boot/efi"))
                 (keyboard-layout keyboard-layout)))
    (file-systems (cons*
                   (file-system
                     (device (uuid "796D-13B3" 'fat))
                     (mount-point "/boot/efi")
                     (type "vfat"))
                   (file-system
                     (device (file-system-label "btrfs-root"))
                     (mount-point "/")
                     (type "btrfs")
                     (options "compress=zstd,subvol=@,noatime"))
                   (file-system
                     (device (file-system-label "btrfs-root"))
                     (mount-point "/gnu")
                     (type "btrfs")
                     (options "compress=zstd,subvol=@gnu,noatime"))
                   (file-system
                     (device (file-system-label "btrfs-root"))
                     (mount-point "/home")
                     (type "btrfs")
                     (options "compress=zstd,subvol=@home,noatime"))
                   (file-system
                     (device (file-system-label "btrfs-root"))
                     (mount-point "/mnt/btrfs-root")
                     (type "btrfs")
                     (options "compress=zstd"))
                   (file-system
                     (mount-point "/mnt/nfs/lucrehulk-data/")
                     (mount? #f)
                     (device "lucrehulk.lan:/data")
                     (options "nolock,user,rw,noauto,noatime,nodiratime,exec")
                     (type "nfs"))
                   %base-file-systems))
    (swap-devices (list (swap-space
                         (target "/mnt/btrfs-root/swapfile")
                         (priority 0)
                         (dependencies (filter (file-system-mount-point-predicate "/mnt/btrfs-root")
                                               file-systems)))))


    (packages (cons* sway nss-certs zsh kitty brightnessctl powerctl
                     %base-packages))
    (setuid-programs
     (cons* (setuid-program
             (program (file-append powerctl "/bin/powerctl")))
            (setuid-program
             (program (file-append inetutils "/bin/traceroute")))
            %setuid-programs))

    (services (cons* (service screen-locker-service-type
                              (screen-locker-configuration
                               (name "swaylock")
                               (program (file-append swaylock "/bin/swaylock"))))
                     (service seatd-service-type)
                     (service bluetooth-service-type)
                     (simple-service 'pbpro-reset-alsa
                                     shepherd-root-service-type
                                     (list (shepherd-service
                                            (documentation "Reset ALSA with a manjaro-derived asound.state")
                                            (provision '(alsa-reset))
                                            (requirement '(user-processes))
                                            (one-shot? #t)
                                            (auto-start? #f)
                                            (respawn? #f)
                                            (start
                                             #~(make-system-constructor
                                                #$(file-append alsa-utils "/sbin/alsactl")
                                                " restore "
                                                " -f "
                                                #$(local-file "./files/pbpro-asound.state"))))))
                     (simple-service 'seatd-xdg-runtime-dirs
                                     shepherd-root-service-type
                                     (list (shepherd-service
                                            (documentation "Make XDG_RUNTIME_DIRs for (at least my) user, since seatd doesn't")
                                            (provision '(xdg-runtime-dir))
                                            (requirement '(user-processes))
                                            (one-shot? #t)
                                            (respawn? #f)
                                            (modules (cons '(ice-9 format) %default-modules))
                                            (start
                                             #~(make-system-constructor
                                                (format #f "set -eu
export XDG_RUNTIME_DIR=/run/user/`~a -u liate`
~a -p $XDG_RUNTIME_DIR
~a liate:users $XDG_RUNTIME_DIR
~a 700 $XDG_RUNTIME_DIR
if ~a $XDG_RUNTIME_DIR; then
exit
elif ~a $XDG_RUNTIME_DIR -maxdepth 0 -empty | ~a .; then
~a -t tmpfs none $XDG_RUNTIME_DIR
fi
"
                                                        #$(file-append coreutils "/bin/id")
                                                        #$(file-append coreutils "/bin/mkdir")
                                                        #$(file-append coreutils "/bin/chown")
                                                        #$(file-append coreutils "/bin/chmod")
                                                        #$(file-append util-linux "/bin/mountpoint")
                                                        #$(file-append findutils "/bin/find")
                                                        #$(file-append grep "/bin/grep")
                                                        #$(file-append util-linux "/bin/mount")))))))

                     (simple-service 'extra-hosts
                                     hosts-service-type
                                     (list (host "8.8.8.8" "test")))

                     (modify-services %desktop-services
                       (delete sddm-service-type)
                       (delete gdm-service-type)
                       (delete elogind-service-type)
                       (sysctl-service-type
                        config => (sysctl-configuration
                                   (settings
                                    (append '(("vm.vfs_cache_pressure" . "500")
                                              ("vm.swappiness" . "100")
                                              ("vm.dirty_background_ratio" . "1")
                                              ("vm.dirty_ratio" . "50"))
                                            %default-sysctl-settings))))
                       (guix-service-type
                        conf => (guix-configuration
                                 (inherit conf)
                                 (substitute-urls
                                  (cons* "http://lucrehulk.lan:8181"
                                         "https://substitutes.nonguix.org"
                                         %default-substitute-urls))
                                 (authorized-keys
                                  (cons* (local-file "./files/nonguix-substitutes-key.pub")
                                         (local-file "./files/lucrehulk-substitutes-key.pub")
                                         (local-file "./files/kalantha-signing-key.pub")
                                         %default-authorized-guix-keys)))))))

    (name-service-switch %mdns-host-lookup-nss)))

river-of-stars
