(define-module (all-packages-manifest)
  #:use-module (guix)
  #:use-module (guix packages)
  #:use-module (guix profiles)

  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)

  #:use-module (ice-9 ftw)
  #:use-module (ice-9 match))

(define* (module-packages module #:optional (ignored-names '()))
  (filter package?
          (module-map (λ (name val)
                        (if (member name ignored-names)
                            #f
                            (variable-ref val)))
                      module)))

(define (name->module name)
  (resolve-module `(liate packages ,(string->symbol (basename name ".scm")))
                  #:ensure #f))

(define scheme-file-basenames
  (match-lambda
    (((? (cute string-suffix? "scm" <>) name) stat)
     name)
    (("patches" stat children ...) '())
    ((name stat children ...)
     (remove (negate string?) (map scheme-file-basenames children)))))

(define unrefreshable-packages
  '(generic-fortunes-jkirchartz aleph-one kandria-demo))

(define-public refreshable-packages-manifest
  (lambda ()
    (packages->manifest
     (concatenate
      (map (compose (cute module-packages <> unrefreshable-packages)
                    name->module)
           (scheme-file-basenames (file-system-tree
                                   (string-append (dirname (current-filename))
                                                  "chan/liate/packages/"))))))))

(refreshable-packages-manifest)
