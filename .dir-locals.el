(("chan"
  .
  ((scheme-mode
	(eval
	 . (with-eval-after-load 'geiser-guile
		 (let ((root-dir (file-name-directory
						  (locate-dominating-file default-directory ".dir-locals.el"))))
		   (require 'cl-lib)
		   (cl-pushnew (concat root-dir "/chan/") geiser-guile-load-path :test #'string-equal))))
	(eval
	 .
	 (setq-local liate/guile-module-root
				 (concat
				  (file-name-directory
				   (locate-dominating-file default-directory ".dir-locals.el"))
				  "/chan/")))))))
