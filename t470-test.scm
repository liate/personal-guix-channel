(use-modules (gnu) (gnu bootloader) (gnu packages) (gnu system) (gnu system setuid)
             (guix gexp) (guix packages)
             (nongnu packages linux)
             (nongnu packages firmware)
             (nongnu system linux-initrd))
(use-service-modules desktop dns networking ssh sddm sound xorg linux sysctl shepherd)
(use-package-modules linux admin firmware cryptsetup shells wm emacs certs terminals
                     base bootloaders)

(define (btrfs-fs-options subvol)
  (string-append "compress=zstd,subvol=" subvol))

(define luks-mapper
  (mapped-device
   (source "/dev/sda2")
   ;; (source (uuid "535b9843-f639-453e-8856-fdafce731f42"))
   (target "root")
   (type luks-device-mapping)))

(operating-system
 (host-name "t470-test")

 (locale "en_US.utf8")
 (keyboard-layout (keyboard-layout "us" #:options '("compose:ralt" "ctrl:swapcaps")))

 (users (cons* (user-account
                (name "liate")
                (comment "Andrew Patterson")
                (group "users")
                (home-directory "/home/liate")
                (shell (file-append zsh "/bin/zsh"))
                (supplementary-groups
                 '("audio" "input" "kvm" "lp" "netdev" "power" "tty" "video" "wheel")))
               %base-user-accounts))
 (groups (cons* (user-group
                 (name "power"))
                %base-groups))

 (kernel linux)
 (initrd microcode-initrd)
 (firmware (cons* linux-firmware %base-firmware))

 (bootloader
  (bootloader-configuration
   (bootloader grub-efi-bootloader)
   (targets '("/boot/efi"))
   (keyboard-layout keyboard-layout)))
 (mapped-devices
  (list luks-mapper))
 (file-systems
  (cons* (file-system
          (mount-point "/boot/efi")
          (device (uuid "A3EA-B3A1" 'fat32))
          (type "vfat"))
         (file-system
          (dependencies (list luks-mapper))
          (mount-point "/")
          (device "/dev/mapper/root")
          (type "btrfs")
          (options (btrfs-fs-options "@guix")))
         (file-system
          (dependencies (list luks-mapper))
          (mount-point "/gnu")
          (device "/dev/mapper/root")
          (type "btrfs")
          (options (btrfs-fs-options "@gnu")))
         (file-system
          (dependencies (list luks-mapper))
          (mount-point "/home")
          (device "/dev/mapper/root")
          (type "btrfs")
          (options (btrfs-fs-options "@home")))
         (file-system
          (dependencies (list luks-mapper))
          (mount-point "/mnt/btrfs-root")
          (device "/dev/mapper/root")
          (type "btrfs")
          (options (btrfs-fs-options "/")))
         %base-file-systems))
 (swap-devices
  (list (swap-space
         (target "/mnt/btrfs-root/@swaps/swapfile")
         (dependencies (filter (file-system-mount-point-predicate "/mnt/btrfs-root")
                               file-systems)))))

 (packages
  (append (map specification->package
               '("sway" "nss-certs" "zsh" "kitty" "guix-simplyblack-sddm-theme"
                 "linux-pam"))
          %base-packages))
 (setuid-programs
  (cons* (setuid-program
          (program (file-append (specification->package "inetutils")
                                "/bin/traceroute")))
         %setuid-programs))

 (services
  (cons* (simple-service 'extra-hosts
                         hosts-service-type
                         (list (host "8.8.8.8" "test")))
         (service sddm-service-type
                  (sddm-configuration
                   (display-server "wayland")
                   (theme "guix-simplyblack-sddm")))
         (modify-services %desktop-services
           (delete gdm-service-type)
           )))
 (name-service-switch %mdns-host-lookup-nss))
